package tests.basic;

import com.fasterxml.jackson.databind.ser.Serializers;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pageobjects.Page;
import pageobjects.YMMainPage;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Title;
import ru.yandex.qatools.allure.model.Description;
import util.Basement;

import java.util.Properties;

public class OpenPageTest {
    @Test
    @Title("Тест на открытие страницы")
    public void OpenPageTest() throws Throwable {
        Basement.getDriver().get(Basement.getConfig().get("homepage").toString());
        Page page = new YMMainPage(Basement.getDriver());
        Assert.assertEquals(page.getTitle(), "Яндекс.Музыка");
    }
}

package util;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Basement {
    private static WebDriver driver = null;
    private static Properties config = null;;

    public static WebDriver getDriver() {
        if (driver == null) {
            System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
            ChromeOptions options = new ChromeOptions();
            options.addArguments("-incognito");
            options.addArguments("dom.webnotifications.enabled");
            //options.addArguments("-start-maximized");

            driver = new ChromeDriver(options);
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        }
        return driver;
    }

    @BeforeSuite
    public static Properties getConfig() throws IOException {
        if (config == null) {
            try {
                config = new Properties();
                config.load(new FileInputStream("config.cfg"));
            } catch (IOException e) {
                return null;
            }
        }
        return config;
    }

    @AfterSuite
    public void shutDown() {
        driver.quit();
    }
}

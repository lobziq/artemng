package pageobjects;

import org.openqa.selenium.WebDriver;

public class YMMainPage extends Page {
    public YMMainPage(WebDriver driver) {
        super(driver, "Главная страница Яндекс.Музыки");
    }
}

package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;
import ru.yandex.qatools.allure.annotations.Title;

public abstract class Page {
    private WebDriver driver = null;
    private String pageName = null;

    public Page(WebDriver driver, String pageName) {
        init(driver, pageName);
    }

    @Step("Открытие страницы [{1}]")
    private void init(WebDriver driver, String pageName) {
        this.pageName = pageName;
        this.driver = driver;
    }

    public String getTitle() {
        return driver.getTitle();
    }
}
